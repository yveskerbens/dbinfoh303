package model.db;

public class SQLQueryFactory {
    private static SQLQueryFactory ourInstance = new SQLQueryFactory();

    public static SQLQueryFactory getInstance() {
        return ourInstance;
    }

    private SQLQueryFactory() {

    }
    public String getCreateAnonymousUserTableQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS ");
        sb.append(DBConstants.ANONYMOUS_USERS_TABLE);
        sb.append("(");
        sb.append(DBConstants.ANONYMOUS_USERS_ID);
        sb.append(" integer NOT NULL, ");
        sb.append(DBConstants.ANONYMOUS_USERS_PASSWORD);
        sb.append(" varchar(30) NOT NULL, ");
        sb.append(DBConstants.ANONYMOUS_USERS_BANKACCOUNT);
        sb.append(" bigint, ");
        sb.append("primary key  (");
        sb.append(DBConstants.ANONYMOUS_USERS_ID);
        sb.append("))");
        System.out.println(sb);
        return sb.toString();
    }


    public String getInsertAnUsersQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(DBConstants.ANONYMOUS_USERS_TABLE);
        sb.append("(");
        sb.append(DBConstants.ANONYMOUS_USERS_ID);
        sb.append(", ");
        sb.append(DBConstants.ANONYMOUS_USERS_PASSWORD);
        sb.append(", ");
        sb.append(DBConstants.ANONYMOUS_USERS_BANKACCOUNT);
        sb.append(") VALUES (?, ?, ?)");
        System.out.println(sb);
        return sb.toString();
    }

    public String getCreateScootersTalbleQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS ");
        sb.append(DBConstants.SCOOTER_TABLE);
        sb.append("(");
        sb.append(DBConstants.SCOOTER_SCOOTER_ID);
        sb.append(" integer NOT NULL, ");
        sb.append(DBConstants.SCOOTER_SCOOTER_COMMISSIONING_DATE);
        sb.append(" timestamptz NOT NULL, ");
        sb.append(DBConstants.SCOOTER_SCOOTER_MODEL);
        sb.append(" char(12), ");
        sb.append(DBConstants.SCOOTER_SCOOTER_COMPLAINT);
        sb.append("  boolean, ");
        sb.append(DBConstants.SCOOTER_SCOOTER_BATTERLEVEL);
        sb.append(" integer, ");
        sb.append("primary key  (");
        sb.append(DBConstants.SCOOTER_SCOOTER_ID);
        sb.append("))");
        return sb.toString();
    }

    public String getInsertScooterQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(DBConstants.SCOOTER_TABLE);
        sb.append("(");
        sb.append(DBConstants.SCOOTER_SCOOTER_ID);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_SCOOTER_COMMISSIONING_DATE);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_SCOOTER_MODEL);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_SCOOTER_COMPLAINT);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_SCOOTER_BATTERLEVEL);
        sb.append(") VALUES (?, ?, ?, ?, ?)");
        System.out.println(sb);
        return sb.toString();
    }

    public String getCreateTripsTableQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("CREATE TABLE IF NOT EXISTS ");
        sb.append(DBConstants.TRIPS_TABLE);
        sb.append("(");
        sb.append(DBConstants.SCOOTER_TRIP_SCOOTER_ID);
        sb.append(" integer NOT NULL, ");
        sb.append(DBConstants.SCOOTER_TRIP_USER_ID);
        sb.append(" integer NOT NULL, ");
        sb.append(DBConstants.SCOOTER_TRIP_SOURCEX);
        sb.append(" double precision, ");
        sb.append(DBConstants.SCOOTER_TRIP_SOURCEY);
        sb.append(" double precision, ");
        sb.append(DBConstants.SCOOTER_TRIP_DESTINATIONX);
        sb.append(" double precision, ");
        sb.append(DBConstants.SCOOTER_TRIP_DESTINATIONY);
        sb.append(" double precision, ");
        sb.append(DBConstants.SCOOTER_TRIP_STARTTIME);
        sb.append(" timestamptz NOT NULL, ");
        sb.append(DBConstants.SCOOTER_TRIP_ENDTIME);
        sb.append(" timestamptz NOT NULL, ");
        sb.append("primary key  (");
        sb.append(DBConstants.SCOOTER_TRIP_STARTTIME);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_SCOOTER_ID);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_USER_ID);
        sb.append("))");
        return  sb.toString();
    }

    public  String getInsertTripsQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(DBConstants.TRIPS_TABLE);
        sb.append("(");
        sb.append(DBConstants.SCOOTER_TRIP_SCOOTER_ID);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_USER_ID);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_SOURCEX);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_SOURCEY);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_DESTINATIONX);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_DESTINATIONY);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_STARTTIME);
        sb.append(", ");
        sb.append(DBConstants.SCOOTER_TRIP_ENDTIME);
        sb.append(") VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        return sb.toString();

    }

}
