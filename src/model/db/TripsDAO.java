package model.db;

import model.DateUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class TripsDAO {
    private static TripsDAO ourInstance = new TripsDAO();

    public static TripsDAO getInstance() {
        return ourInstance;
    }

    private TripsDAO() {}

    public  void insertTrips(List<String[]> trips) {
        try {
            Connection connection = DBConnection.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    SQLQueryFactory.getInstance().getInsertTripsQuery());
            trips.remove(0);
            for(String[] tripRow: trips) {
                preparedStatement.setInt(1, Integer.parseInt(tripRow[0]));
                preparedStatement.setInt(2, Integer.parseInt(tripRow[1]));
                preparedStatement.setDouble(3, Double.parseDouble(tripRow[2]));
                preparedStatement.setDouble(4, Double.parseDouble(tripRow[3]));
                preparedStatement.setDouble(5, Double.parseDouble(tripRow[4]));
                preparedStatement.setDouble(6, Double.parseDouble(tripRow[5]));
                preparedStatement.setTimestamp(7, DateUtil.getInstance().getdate(tripRow[6]));
                preparedStatement.setTimestamp(8, DateUtil.getInstance().getdate(tripRow[7]));
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e){
            System.out.println("Failed to insert data, Connection issue with dB");
        }
        System.out.println("trips intersion done ");
    }
}
