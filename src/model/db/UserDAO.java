package model.db;
import model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class UserDAO {
    private static UserDAO ourInstance = new UserDAO();

    public static UserDAO getInstance() {
        return ourInstance;
    }

    private UserDAO() {
    }

    public  void insertAnonymousUsers(List<User> users){
        try {
            Connection connection = DBConnection.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    SQLQueryFactory.getInstance().getInsertAnUsersQuery());
            for(User user: users) {
                preparedStatement.setInt(1, Integer.parseInt(user.getID()));
                preparedStatement.setString(2, user.getPassword());
                preparedStatement.setLong(3, Long.valueOf(user.getBankaccount()));
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e){
            System.out.println("Failed to insert data, Connection issue with dB");
            e.printStackTrace();
        }
        System.out.println("trips intersion done ");
    }
}
