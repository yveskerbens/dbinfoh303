package model.db;
import model.DateUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class ScootersDAO {
    private static ScootersDAO ourInstance = new ScootersDAO();

    public static ScootersDAO getInstance() {
        return ourInstance;
    }

    private ScootersDAO() {
    }

    public  void insertScooters(List<String[]> scooters){
        try {scooters.remove(0);

            Connection connection = DBConnection.getInstance().getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(
                    SQLQueryFactory.getInstance().getInsertScooterQuery());
            for(String[] tripRow: scooters) {
                preparedStatement.setInt(1, Integer.parseInt(tripRow[0]));
                preparedStatement.setTimestamp(2, DateUtil.getInstance().getdate((tripRow[1])));
                preparedStatement.setString(3, tripRow[2]);
                preparedStatement.setBoolean(4, Boolean.valueOf(tripRow[3]));
                preparedStatement.setInt(5, Integer.parseInt(tripRow[4]));
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e){
            System.out.println("Failed to insert data, Connection issue with dB");
        }
        System.out.println("trips intersion done ");
    }

}
