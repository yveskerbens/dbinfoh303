package model.db;

public class DBConstants {
    public static final String SCOOTERS_DB = "scooters";

    //Table trips
    public static final String TRIPS_TABLE = "infoh303.trips";
    public static final String SCOOTER_TRIP_SCOOTER_ID= "scooterID";
    public static final String SCOOTER_TRIP_USER_ID= "userID";
    public static final  String SCOOTER_TRIP_SOURCEX= "sourceX";
    public static final  String SCOOTER_TRIP_SOURCEY= "sourceY";
    public static final  String SCOOTER_TRIP_DESTINATIONX= "destinationX";
    public static final  String SCOOTER_TRIP_DESTINATIONY= "destinationY";
    public static final  String SCOOTER_TRIP_STARTTIME= "starttime";
    public static final  String SCOOTER_TRIP_ENDTIME= " endtime";

    //Table scooters
    public static final String SCOOTER_TABLE = "infoh303.scooters";
    public static final String  SCOOTER_SCOOTER_ID = "ID";
    public static final String  SCOOTER_SCOOTER_COMMISSIONING_DATE = "commissioning_date";
    public static final String  SCOOTER_SCOOTER_MODEL = "model";
    public static final String  SCOOTER_SCOOTER_COMPLAINT = "complain";
    public static final String  SCOOTER_SCOOTER_BATTERLEVEL = "battery_level";

    //Table reparations

    //Table Reloads

    //Table anonymous_user
    public static final String ANONYMOUS_USERS_TABLE = "infoh303.anonymoususers";
    public static final String ANONYMOUS_USERS_ID = "ID";
    public static final String ANONYMOUS_USERS_PASSWORD ="password";
    public static final String ANONYMOUS_USERS_BANKACCOUNT = "bankaccount";
    // Table Registered User

    //Table Mechanician

}