package model.db;

public class MechanicianDAO {
    private static MechanicianDAO ourInstance = new MechanicianDAO();

    public static MechanicianDAO getInstance() {
        return ourInstance;
    }

    private MechanicianDAO() {
    }
}
