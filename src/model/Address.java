package model;

public class Address {

    private String city;
    private int cp;
    private String street;
    private int number;

    public Address(String city, int cp, String street, int number) {
        this.city = city;
        this.cp = cp;
        this.street = street;
        this.number = number;
    }

    public String getCity() {
        return city;
    }

    public int getCp() {
        return cp;
    }
    public String getStreet() {
        return street;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return city + '\'' +
                ", " + cp +
                ",  " + street + '\'' +
                ", " + number;
    }
}
