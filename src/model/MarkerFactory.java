package model;

import com.lynden.gmapsfx.javascript.object.LatLong;
import com.lynden.gmapsfx.javascript.object.Marker;
import com.lynden.gmapsfx.javascript.object.MarkerOptions;

public class MarkerFactory {
    private static MarkerFactory ourInstance = new MarkerFactory();

    public static MarkerFactory getInstance() {
        return ourInstance;
    }

    private MarkerFactory() {

    }

    public Marker build(double lat, double lon, String title){
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position( new LatLong(lat, lon) )
                .visible(Boolean.TRUE)
                .title(title);
        return  new Marker( markerOptions);
    }
}
