package model;

public class User {
    private String ID;
    private String password;
    private String bankaccount;

    public User(String ID, String password, String bankaccount) {
        this.ID = ID;
        this.password = password;
        this.bankaccount = bankaccount;
    }

    public String getID() {
        return ID;
    }


    public String getPassword() {
        return password;
    }


    public String getBankaccount() {
        return bankaccount;
    }

    @Override
    public String toString() {
        return "User{" +
                "ID='" + ID + '\'' +
                ", password='" + password + '\'' +
                ", bankaccount='" + bankaccount + '\'' +
                '}';
    }
}
