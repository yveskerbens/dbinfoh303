package model;

public abstract class Person extends User{
    private Address address;
    private String firstName;
    private String lastName;
    private String phone;

    public Person(String ID, String password,
                  String bankaccount, Address address,
                  String firstName, String lastName, String phone) {
        super(ID, password, bankaccount);
        this.address = address;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
    }

    public String getID() {
        return super.getID();
    }


    public String getPassword() {
        return super.getPassword();
    }


    public String getBankaccount() {
        return super.getBankaccount();
    }

    public Address getAddress() {
        return address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhone() {
        return phone;
    }


    @Override
    public String toString() {
        return " " + address +
                ",  " + firstName + '\'' +
                ",  " + lastName + '\'' +
                ", '" + phone ;
    }
}
