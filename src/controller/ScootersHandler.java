package controller;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import model.Scooter;

public class ScootersHandler {
    private AnchorPane googleMapHolder;

    private ListView<Scooter> scooterListView;

    final GoogleMapHandler mapHandler;

    public ScootersHandler(AnchorPane googleMapHolder, ListView<Scooter> scooterListView) {
        this.googleMapHolder = googleMapHolder;
        this.scooterListView = scooterListView;
        mapHandler =  new GoogleMapHandler(googleMapHolder);
    }

    public void buildModule() {
        mapHandler.setMapView();
    }

    private void getCooters(){

    }
}
