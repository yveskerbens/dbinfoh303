package controller;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.object.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.layout.AnchorPane;
import model.MarkerFactory;

public class GoogleMapHandler implements MapComponentInitializedListener {

    private AnchorPane googleMapHodler;
    private GoogleMapView googleMapView;
    private GoogleMap map;
    private static  ObservableList<Marker> markers;

    public GoogleMapHandler(AnchorPane googleMapHodler) {
        this.googleMapHodler = googleMapHodler;
        this.googleMapView = new GoogleMapView("en-US",
                "AIzaSyC7t-jSo-huNGoyxN3hlFIBrvLiZGZorPA");
        this.markers = FXCollections.observableArrayList();
    }

    public void setMapView(){
        googleMapHodler.getChildren().add(googleMapView);
        googleMapView.setPrefHeight(470);
        googleMapView.setPrefWidth(700);
        googleMapView.addMapInializedListener(this);
    }

    @Override
    public void mapInitialized() {
        setMapInit();
        setUpMapMarker();
    }

    private void setMapInit() {
        MapOptions mapOptions = new MapOptions();
        mapOptions.center(new LatLong(50.8503 , 4.3517)).mapType(MapTypeIdEnum.ROADMAP)
                .overviewMapControl(false).panControl(false).rotateControl(false).scaleControl(false)
                .streetViewControl(false).zoomControl(false).zoom(12);
        map = googleMapView.createMap(mapOptions);
    }

    private void setUpMapMarker(){
        markers.addListener((ListChangeListener<Marker>) c -> {
            while (c.next()){
                int start = c.getFrom() ; int end = c.getTo() ;
                for (int i = start ; i < end ; i++) {
                    map.addMarker(c.getList().get(i));
                    System.out.println(  c.getList().get(i).getTitle());
                }
            }
        });

        // FIXME: 4/20/19 to be removed
        markers.add(MarkerFactory.getInstance().build(50.8503, 4.3517, "Scooter1"));
        markers.add(MarkerFactory.getInstance().build(50.86946, 4.36523, "Scooter2"));
        markers.add(MarkerFactory.getInstance().build(50.86633, 4.37391, "Scooter3"));
        markers.add(MarkerFactory.getInstance().build(50.82679, 4.36688, "Scooter4"));
    }

    /**
     * Adding scooters location to the  list of markers
     * @param markers
     */
    public static void setMarkers(Marker markers) {
        GoogleMapHandler.markers.add(markers);
    }
}
