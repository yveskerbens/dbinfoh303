package controller;

import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import model.GraphicUtil;
import model.GuilUrl;
import model.Scooter;

public class MainController {

    @FXML
    private AnchorPane googleMapHolder;

    @FXML
    private ListView<Scooter> scooterListView;

    @FXML ListView<String> facettedSearchResult;

    @FXML
    private ImageView generalSearchIcon;

    @FXML
    private TextField scootersSearchField;

    @FXML
    private Button scootersSearchBtn, allScootersSearchBtn;

    public void initialize() {
        final ScootersHandler scooterHandler = new ScootersHandler(googleMapHolder, scooterListView);
        scooterHandler.buildModule();
        setIcon();
        tune();
    }

    private void setIcon(){
        GraphicUtil.getInstance().setIcon(generalSearchIcon, GuilUrl.SEARCH_ICON);
    }

    private void tune(){
        allScootersSearchBtn.setAlignment(Pos.BASELINE_LEFT);
    }
}

