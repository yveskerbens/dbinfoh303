package model;

import model.db.UserDAO;
import model.parser.XMLParser;
import org.jdom.JDOMException;
import org.junit.Assert;
import org.junit.Test;
import java.io.File;
import java.io.IOException;
import java.util.List;


public class XMLParserTest {
    @Test
    public void parseUserFileTest() throws JDOMException, IOException {
        File inputFile = new File("src/model/db/resource/anonyme_users.xml");
        List<User> users = XMLParser.getInstance().parse(inputFile);
        Assert.assertEquals(544, users.size());
        UserDAO.getInstance().insertAnonymousUsers(users);
    }

    @Test
    public void testRegisteredUser() throws JDOMException, IOException {
        File inputFile = new File("src/model/db/resource/registeredUsers.xml");
        List<Person>  people= XMLParser.getInstance().parse("registeredUser", inputFile);
        Assert.assertEquals(545, people.size());
    }

    @Test
    public void parseMechanicianTest() throws JDOMException, IOException {
        File inputFile = new File("src/model/db/resource/mecaniciens.xml");
        List<Person>  people= XMLParser.getInstance().parse("mechanician", inputFile);
        Assert.assertEquals(7, people.size());
    }
}