package model;
import model.db.SQLQueryFactory;
import model.db.ScootersDAO;
import model.db.TripsDAO;
import model.parser.CSVFileParser;
import org.junit.Test;
import java.io.FileReader;
import java.util.List;

public class CSVParserTest {

    @Test
    public void readTripsTest() throws Exception {
        List<String[]> csvData= CSVFileParser.getInstance().parseCSVFileWithCommaSeparator(new FileReader("../dbinfoh303/resource/trips.csv"));
        TripsDAO.getInstance().insertTrips(csvData);
        readLine(csvData);
    }

    @Test
    public void readScootersTest() throws Exception {
        List<String[]> csvData= CSVFileParser.getInstance().parseCSVFileWithSemiColonSeparator(new FileReader("src/model/db/resource/scooters.csv"));
        System.out.println(SQLQueryFactory.getInstance().getCreateScootersTalbleQuery());
        ScootersDAO.getInstance().insertScooters(csvData);
        //readLine(csvData);
    }



    @Test
    public void readReloadsTest() throws Exception {
        List<String[]> csvData= CSVFileParser.getInstance().parseCSVFileWithCommaSeparator(new FileReader("src/model/db/resource/reloads.csv"));
        readLine(csvData);
        System.out.println();
    }



    @Test
    public void readReparationsTest() throws Exception {
        List<String[]> csvData= CSVFileParser.getInstance().parseCSVFileWithCommaSeparator(new FileReader("src/model/db/resource/reparations.csv"));
        readLine(csvData);
        System.out.println();
    }

    private void readLine(List<String[]> csvData) {
        for(String[] rows: csvData){
            System.out.println("--------------------------------------------------------------------------------------------------------");
            for(String str: rows){
                System.out.print( str);
                System.out.print("\t\t");
            }
            System.out.println();
            System.out.println();

        }
    }

}